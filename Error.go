package kafka_go

import (
	"errors"
)

var (
	//--消费者错误---
	//构建器不能为空
	KAFKA_CONSUMER_ERROR_BUILD_EMPTY = errors.New("consumer build len can not be empty")

	//生产者错误
	//构建器不能为空
	KAFKA_PRODUCT_ERROR_BUILD_EMPTY = errors.New("product build len can not be empty")
	//构建器名称不能为空
	KAFKA_PRODUCT_ERROR_BUILD_NAME_EMPTY = errors.New("product build name can not be empty")
	//连接不存在
	KAFKA_PRODUCT_ERROR_BUILD_CLIENT_EMPTY = errors.New("product client empty")
	//客户端连接错误
	KAFKA_PRODUCT_ERROR_BUILD_CLIENT_NIL = errors.New("product client nil")
)
