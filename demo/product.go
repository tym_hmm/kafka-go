package main

import (
	"demo/Conf"
	"fmt"
	kafka_go "gitee.com/tym_hmm/kafka-go"
	"log"
)

var (
	productFactory = kafka_go.NewFactoryProduct()
)

func init() {
	demoBuild := kafka_go.NewBuildProduct("demo1", Conf.Addr).SetAckType(kafka_go.PRODUCT_ACK_TYPE_ALL).SetDebug(true).SetMaxConnection(4)
	err := productFactory.Register(demoBuild).Connect()
	if err != nil {
		log.Panicln(err)
	}
}

func main() {
	for {
		partition, offset, err := productFactory.Push("demo1", Conf.Topic3, "生产者消息213213")
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Printf("发送成功  partition:%d, offset:%d\n", partition, offset)
		}
	}

	//num := 100000
	//var wg = &sync.WaitGroup{}
	//for i := 0; i < num; i++ {
	//	wg.Add(1)
	//	go func() {
	//		defer wg.Done()
	//		partition, offset, err := productFactory.Push("demo1", Conf.Topic3, "生产者消息213213")
	//		if err != nil {
	//			fmt.Println(err)
	//		} else {
	//			fmt.Printf("发送成功  partition:%d, offset:%d\n", partition, offset)
	//		}
	//	}()
	//
	//}
	//wg.Wait()
}
