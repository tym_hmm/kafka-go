package test

import (
	"demo/Conf"
	"fmt"
	kafka_go "gitee.com/tym_hmm/kafka-go"
	"log"
	"sync"
	"testing"
)

var productFactory = kafka_go.NewFactoryProduct()

func init() {
	demoBuild := kafka_go.NewBuildProduct("demo1", Conf.Addr).SetDebug(true).SetMaxConnection(4)

	demo2Build := kafka_go.NewBuildProduct("demo2", Conf.Addr).SetDebug(true).SetMaxConnection(4)
	err := productFactory.Register(demoBuild, demo2Build).Connect()
	if err != nil {
		log.Panicln(err)
	}
}

//生产者测试
func TestProduct(t *testing.T) {
	num := 4
	var wg = &sync.WaitGroup{}
	for i := 0; i < num; i++ {
		_i := i
		wg.Add(1)
		go func(_num int) {
			defer wg.Done()
			partition, offset, err := productFactory.Push("demo1", topic3, fmt.Sprintf("生产者消息 %s number:%d", "demo1", _num))
			if err != nil {
				t.Errorf("发送失败")
			} else {
				t.Logf("demo1 发送成功 partition:%d, offset:%d", partition, offset)
			}
		}(_i)

		wg.Add(1)
		go func(_num int) {
			defer wg.Done()
			partition, offset, err := productFactory.Push("demo2", topic3, fmt.Sprintf("生产者消息 %s number:%d", "demo2", _num))
			if err != nil {
				t.Errorf("发送失败")
			} else {
				t.Logf("demo2 发送成功 partition:%d, offset:%d", partition, offset)
			}
		}(_i)
		//time.Sleep(time.Second)
	}
	wg.Wait()
}
