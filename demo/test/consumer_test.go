package test

import (
	"fmt"
	kafka_go "gitee.com/tym_hmm/kafka-go"
	"testing"
	"time"
)

////消息提交确认方式
////接收到消息就确认,无需等待逻辑响应
//ACK_LEVLE_ACK AckLevel = 0
//
////接收消息，等待主节点响应，无需等待broker
//ACK_LEVEL_LOCAL AckLevel = 1
//
////接收消息，等待主节点和brokder完完毕
//ACK_LEVEL_ALL AckLevel = -1

var (
	addr    = "192.168.186.130:9092,192.168.186.201:9092,192.168.186.202:9092"
	topic   = "web_log"
	groupId = "testGroupId"

	groupId2 = "testGroupId2"
	topic2   = "weg2_log"

	topic3   = "web3_log"
	groupId3 = "testGroupId"
)

var (
	consumerFactory = kafka_go.NewFactoryConsumer()
	UTFALL_SECOND   = "2006-01-02 15:04:05"
	cstZone         = time.FixedZone("CST", 8*3600)
)

func TestConsumer(t *testing.T) {
	demoConsumerBuilder := kafka_go.NewBuildConsumer(addr, groupId, topic)
	demoConsumerBuilder.SetKafkaVersion("3.0.0")
	demoConsumerBuilder.SetResponseListener(func(context *kafka_go.ConsumerMessageContext) {
		nowTime := time.Now().In(cstZone).Format(UTFALL_SECOND)
		fmt.Printf("%s xx received [content:%s]\n", nowTime, context.GetMessageString())
		//fmt.Printf("%+v\n", context.GetMessageString())
		context.GetSession().Ack()
	})

	demo2ConsumerBuilder := kafka_go.NewBuildConsumer(addr, groupId2, topic2)
	demo2ConsumerBuilder.SetKafkaVersion("3.0.0")
	demo2ConsumerBuilder.SetResponseListener(func(context *kafka_go.ConsumerMessageContext) {
		nowTime := time.Now().In(cstZone).Format(UTFALL_SECOND)
		fmt.Printf("%s xx received2 [content:%s]\n", nowTime, context.GetMessageString())
		context.GetSession().Ack()
	})

	consumerFactory.RegisterConsumer(demoConsumerBuilder, demo2ConsumerBuilder)
	consumerFactory.Run()
}
