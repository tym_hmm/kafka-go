package Stragey

import (
	"sync"
)

type strategyContext struct {
	index int32
	max   int32
	api   StrategyApi
}

func (this *strategyContext) reset() {
	this.index = 0
	this.max = 0
}

func (this *strategyContext) handle() int32 {
	return this.api.Exec(this.index, this.max)
}

/**
策略处理
*/
type strategyFactory struct {
	pool sync.Pool
}

func NewStrategyFactory() *strategyFactory {
	stf := &strategyFactory{}
	stf.pool.New = func() any {
		return &strategyContext{}
	}
	return stf
}

func (this *strategyFactory) param(index, max int32, api StrategyApi) *strategyContext {
	_c := this.pool.Get().(*strategyContext)
	_c.index = index
	_c.max = max
	_c.api = api
	paramC := *_c
	_c.reset()
	this.pool.Put(_c)
	return &paramC
}

/**
轮询
*/
func (this *strategyFactory) RoundRobin(index, max int32) int32 {
	return this.param(index, max, newStrategyRoundRobin()).handle()
}
