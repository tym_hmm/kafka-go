package Stragey

/**
策略算法-轮询
*/
type strategyRoundRobin struct {
	StrategyBase
}

func newStrategyRoundRobin() *strategyRoundRobin {
	return &strategyRoundRobin{}
}

func (this *strategyRoundRobin) Exec(index, max int32) int32 {
	this.SetMax(max)
	this.SetIndex(index)
	return this.handle()
}

func (this *strategyRoundRobin) handle() int32 {
	if this.GetMax() == 0 {
		return 0
	}
	return (this.GetIndex() + 1) % this.GetMax()
}
