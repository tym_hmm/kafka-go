package Stragey

type StrategyBase struct {
	index int32
	max   int32
}

func (this *StrategyBase) SetIndex(index int32) {
	this.index = index
}
func (this *StrategyBase) GetIndex() int32 {
	return this.index
}

func (this *StrategyBase) SetMax(max int32) {
	this.max = max
}
func (this *StrategyBase) GetMax() int32 {
	return this.max
}

/**
抽象处理
*/
type StrategyApi interface {
	GetIndex() int32
	GetMax() int32
	/**
	策略执行
	@param index
	@param max
	@return int32
	*/
	Exec(index, max int32) int32
}
