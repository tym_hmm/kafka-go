package kafka_go

import (
	sarama "gitee.com/tym_hmm/go-kafa-shopify-sarama"
)

/**
构建消费者接口
*/
type BuildConsumerApi interface {
	SetDebug(isDebug bool) BuildConsumerApi
	IsDebug() bool
	//设置kafka版本号
	SetKafkaVersion(kafkaVersion string) BuildConsumerApi
	//设置其它配置
	SetConfig(config *sarama.Config) BuildConsumerApi
	//是否分区策略类型
	SetBalanceType(balanceType BalanceType) BuildConsumerApi
	//是否多个分区加载
	IsMultiplePartition() bool
	/*
		是否多个分区加载
		消费者多个分区不保证消息稳定性，
		但可以保证吞吐量
	*/
	SetMultiplePartition(isMultiplePartition bool) BuildConsumerApi
	//是否自动提交
	SetIsAutoCommit(autoCommit bool) BuildConsumerApi
	//设置消息返回事件
	SetResponseListener(responseResult ConsumerResponseListener) BuildConsumerApi
	//返回消息事件监听
	GetResponseListener() ConsumerResponseListener
	//获取broker地址
	GetAddr() string
	//获取组id
	GetGroupId() string
	//获取主题
	GetTopic() string
	//获取分区策略方式
	GetBalanceType() BalanceType
	//是否启用加载历史消费
	IsOldOffset() bool
	//是否自动提交
	IsAutoCommit() bool
	//获取版本
	GetKafkaVersion() string
	//获取原始配置
	GetConfig() *sarama.Config
	//返回构建数据
	ToString() string
}

/**
生产者构建接口
*/
type BuildProductApi interface {
	//是否开启调试
	SetDebug(isDebug bool) BuildProductApi
	//是否开启事务提交
	SetTransactional(isTransactional bool) BuildProductApi
	GetTransactional() bool
	//设置确认方式
	SetAckType(ackType ProductAckType) BuildProductApi
	//当前buildName
	GetName() string
	//生产者连接地址
	GetAddr() string
	//获取连接地址数组
	GetAddrSlice() []string
	//设置配置
	SetConfig(config *sarama.Config) BuildProductApi
	//设置连接数
	SetMaxConnection(maxConnection int32) BuildProductApi
	//获取连接策略模式
	GetConnStrategy() ProductBalanceType
	//获取连接数
	GetMaxConnection() int32
	//获取配置
	GetConfig() *sarama.Config
	//获取build hashcode
	GetHashCode() int64
	GetHashCodeString() string
	//返回构建数据
	ToString() string
}
